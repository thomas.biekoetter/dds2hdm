from setuptools import setup


with open("README.md", "r") as fh:
    long_description = fh.read()


setup(
    name = "dds2hdm",
    version = "1.0.0",
    description = "Scattering of dark matter on nuclei in the S2HDM.",
    long_description = long_description,
    packages = ["dds2hdm"],
    author = ["Thomas Biekotter"],
    author_email = [
        "thomas.biekoetter@desy.de"],
    url = "https://gitlab.com/thomas.biekoetter/dds2hdm",
    include_package_data = True,
    package_data = {"dds2hdm": ["*"]},
    install_requires = ["numpy"],
    python_requires = '>=3.6',
)
