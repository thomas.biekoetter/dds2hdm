# Package name
PACKAGE_NAME = dds2hdm
PROJECT_DIR = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))


# output formatting
BOLD := \033[1m
RESET := \033[0m


# Compilers
PC = python
FC = gfortran


# check if version 3
PYV_MIN_MAIN = 3
PYV_MIN_MINOR = 6
PYV_MAIN = $(shell $(PC) -c 'import sys; print("%d"% sys.version_info[0])' )
PYV_MINOR = $(shell $(PC) -c 'import sys; print("%d"% sys.version_info[1])' )
PYV_OK_MAIN = $(shell $(PC) -c 'import sys;\
  print(int(float("%d"% sys.version_info[0]) >= $(PYV_MIN_MAIN)))' )
PYV_OK_MINOR = $(shell $(PC) -c 'import sys;\
  print(int(float("%d"% sys.version_info[1]) >= $(PYV_MIN_MINOR)))' )
ifeq ($(PYV_OK_MAIN),0)
  $(error "python points to version $(PYV_MAIN), but version >$(PYV_MIN_MAIN) is required.")
endif
ifeq ($(PYV_OK_MINOR),0)
  $(error "python points to version $(PYV_MAIN).$(PYV_MINOR), but version >$(PYV_MIN_MAIN).$(PYV_MIN_MINOR) is required.")
endif


# External sources

# Looptools
LT_NAME = LoopTools-2.15
LT_TAR = $(LT_NAME).tar.gz
LT_URL = https://feynarts.de/looptools/$(LT_TAR)


# Download external sources

download-looptools:
	@echo
	@echo "$(BOLD)Downloading $(LT_NAME) from $(LT_URL)...$(RESET)"
	bash -c "cd ./external/ && mkdir -p $(LT_NAME) && wget $(LT_URL)" 
	bash -c "cd ./external/ && tar -xvzf $(LT_TAR) && rm $(LT_TAR)"
	@echo "$(BOLD)Done...$(RESET) (downloaded: $(LT_NAME))"


# Build external sources

build-looptools:
	@echo
	@echo "$(BOLD)Building $(LT_NAME)...$(RESET)"
	bash -c "cd ./external/$(LT_NAME) && cp ../LT_mods/configure ."
	bash -c "cd ./external/$(LT_NAME) && cp ../LT_mods/ffinit.F src/util/."
	bash -c "cd ./external/$(LT_NAME) && ./configure FC=$(FC)"
	bash -c "cd ./external/$(LT_NAME) && make clean"
	bash -c "cd ./external/$(LT_NAME) && make && make install"
	@echo "$(BOLD)Done...$(RESET) (build: $(LT_NAME))"


# Build internal sources

build-internal:
	@echo
	@echo "$(BOLD)Building fortran module...$(RESET)"
	bash -c "cd ./dds2hdm && make all PC=$(PC)"
	@echo "$(BOLD)Done...$(RESET)"


# Install as package

install-python-prerequisites:
	@echo
	@echo "$(BOLD)Installing/upgrading prerequisites in base environment ($(PC))...$(RESET)"
	@$(PC) -m pip install -U --no-cache-dir pip setuptools numpy
	@echo "$(BOLD)Done...$(RESET) (installed: pip, setuptools and numpy)"

install:
	@echo
	@echo "$(BOLD)Installing $(PACKAGE_NAME) in base environment ($(PC))...$(RESET)"
	@$(PC) -m pip install -U --no-cache-dir .
	@echo "$(BOLD)Done...$(RESET) (installed: $(PACKAGE_NAME))"


# Make everything

all: install-python-prerequisites download-looptools
all: build-looptools build-internal install


# Cleaning

clean:
	@echo
	@echo "$(BOLD)Cleaning...$(RESET)"
	bash -c "git clean -f -xdf"
	@echo "$(BOLD)Done...$(RESET)"


$(shell mkdir -p external)
