import numpy as np

import dds2hdm.interface


paras = {
    'type': 2,
    'tb': 2.0,
    'al1': 1.2,
    'al2': 1.3,
    'al3': 1.4,
    'mH1': 100.0,
    'mH2': 200.0,
    'mH3': 300.0,
    'mXi': 100.0,
    'mA': 500.0,
    'mHp': 600.0,
    'vs': 300.0,
    'm12sq': 600.0**2 * np.sin(np.arctan(2.0)) * np.cos(np.arctan(2.0))}

dd = dds2hdm.interface.FromAngleInput(
    paras,
    Divergence=1,
    MuDimsq=paras['mXi']**2)

xs = dd.get_cross_section()
print(dd.Fuu)
print(dd.Fdd)
print(xs)
