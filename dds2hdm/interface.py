import numpy as np

import dds2hdm.typeii
import dds2hdm.typei


ImagError = RuntimeError(
    'Imaginary parts. Not sure if this should happen...')


vevSM = 246.221
Gf = 1.16637e-5
alew = 1. / 127.9
mZ = 91.1876
mW = np.sqrt(
    mZ**2 / 2. + np.sqrt(
        mZ**4 / 4. - np.pi / np.sqrt(2.) * alew / Gf * mZ**2))


mNuc = 1.0
convGeVcmSq = (1.98e-14)**2


fTqProton = np.array([0.029, 0.027, 0.009])
fTqNeutron = np.array([0.013, 0.040, 0.009])


class S2hdmTools:

    def __init__(
            self, pt,
            MuDimsq=None, Divergence=1.0,
            nucleon_type='proton'):
        self.mDM = pt.mXi
        if not nucleon_type in ['proton', 'neutron']:
            raise ValueError(
                "Optional argument nucleon_type either 'proton' or 'neutron'")
        else:
            self.calc_nucleon_facs(nucleon_type)
        self.yuktype = pt.yuktype
        self.Mh1sq = pt.mH1**2
        self.Mh2sq = pt.mH2**2
        self.Mh3sq = pt.mH3**2
        self.MA02 = pt.mA**2
        self.MXisq = pt.mXi**2
        self.MHp2 = pt.mHp**2
        self.MW2 = pt.mW**2
        self.MZ2 = pt.mZ**2
        self.Rot = pt.R
        self.lams = np.array([
            pt.lam1, pt.lam2, pt.lam3, pt.lam4,
            pt.lam5, pt.lam6, pt.lam7, pt.lam8])
        self.vevs = np.array([
            pt.v1, pt.v2, pt.vs])
        self.CB2 = pt.cb**2
        self.SB2 = pt.sb**2
        self.Divergence = Divergence
        if not MuDimsq is None:
            self.MuDimsq = MuDimsq
        else:
            self.MuDimsq = self.MXisq
        # self.muANSq = (pt.mXi * mNuc / (pt.mXi + mNuc))**2
        self.Fuu = None
        self.Fdd = None

    def calc_nucleon_facs(self, n):
        if n == 'proton':
            self.fTq = fTqProton
        else:
            self.fTq = fTqNeutron
        self.fTg = 1.0 - np.sum(self.fTq)
        self.xsprefac = convGeVcmSq * mNuc**4 / ((mNuc + self.mDM)**2 * np.pi)
        self.fTgfac = 2.0 * self.fTg / 27.0

    def get_cross_section(self):
        xs = 0.0
        if self.Fuu is None:
            self.Fuu, self.Fdd = self.calc_oneloop_F()
        # xs = convGeVcmSq * (NuclFacSq / np.pi) * \
        #     self.muANSq / self.MXisq * self.F**2
        sumuds = \
            self.Fuu * self.fTq[0] + \
            self.Fdd * self.fTq[1] + \
            self.Fdd * self.fTq[2]
        sumbct = \
            self.Fdd + \
            self.Fuu + \
            self.Fuu
        xs = self.xsprefac * (sumuds + self.fTgfac * sumbct)**2
        return xs

    def calc_oneloop_F(self):
        if self.yuktype in [2, 4]:
            res = dds2hdm.typeii.typeii.evaluate(
                self.Mh1sq, self.Mh2sq, self.Mh3sq, self.MA02,
                self.MXisq, self.MHp2, self.MW2, self.MZ2,
                self.Rot, self.lams, self.vevs,
                self.CB2, self.SB2,
                self.MuDimsq, self.Divergence)
        else:
            res = dds2hdm.typei.typei.evaluate(
                self.Mh1sq, self.Mh2sq, self.Mh3sq, self.MA02,
                self.MXisq, self.MHp2, self.MW2, self.MZ2,
                self.Rot, self.lams, self.vevs,
                self.CB2, self.SB2,
                self.MuDimsq, self.Divergence)
        self.calcsuure = res[4]
        self.calcsuuim = res[5]
        self.calcsddre = res[6]
        self.calcsddim = res[7]
        for x1, x2 in zip(self.calcsuure, self.calcsuuim):
            if abs(x2 / x1) > 1e-3:
                print(x1, x2)
                raise ImagError
        for x1, x2 in zip(self.calcsddre, self.calcsddim):
            if abs(x2 / x1) > 1e-3:
                print(x1, x2)
                raise ImagError
        if abs(res[1] / res[0]) < 1e-3:
             Fuu = res[0]
        else:
            print(res)
            raise ImagError
        if abs(res[3] / res[2]) < 1e-3:
             Fdd = res[2]
        else:
            print(res)
            raise ImagError
        return [Fuu, Fdd]


class FromAngleInput(S2hdmTools):

    def __init__(
            self, dc, debug=True,
            MuDimsq=None, Divergence=1.0,
            nucleon_type='proton'):
        self.mDM = dc['mXi']
        if not nucleon_type in ['proton', 'neutron']:
            raise ValueError(
                "Optional argument nucleon_type either 'proton' or 'neutron'")
        else:
            self.calc_nucleon_facs(nucleon_type)
        self.dc = dc
        self.read_constants()
        self.read_angle_input()
        self.get_phi_vevs()
        self.get_lambdas()
        self.get_bilinears()
        if debug:
            self.check_masses()
        self.Mh1sq = self.mH1**2
        self.Mh2sq = self.mH2**2
        self.Mh3sq = self.mH3**2
        self.MA02 = self.mA**2
        self.MXisq = self.mXi**2
        self.MHp2 = self.mHp**2
        self.MW2 = self.mW**2
        self.MZ2 = self.mZ**2
        self.Rot = self.R
        self.lams = np.array([
            self.lam1, self.lam2, self.lam3, self.lam4,
            self.lam5, self.lam6, self.lam7, self.lam8])
        self.vevs = np.array([
            self.v1, self.v2, self.vs])
        self.CB2 = self.cb**2
        self.SB2 = self.sb**2
        self.Divergence = Divergence
        if not MuDimsq is None:
            self.MuDimsq = MuDimsq
        else:
            self.MuDimsq = self.MXisq
        # self.muANSq = (self.mXi * mNuc / (self.mXi + mNuc))**2
        self.Fuu = None
        self.Fdd = None

    def read_constants(self):
        self.v = vevSM
        self.mW = mW
        self.mZ = mZ

    def read_angle_input(self):
        dc = self.dc
        self.yuktype = dc['type']
        self.tb = dc['tb']
        self.al1 = dc['al1']
        self.al2 = dc['al2']
        self.al3 = dc['al3']
        self.mH1 = dc['mH1']
        self.mH2 = dc['mH2']
        self.mH3 = dc['mH3']
        self.mXi = dc['mXi']
        self.mA = dc['mA']
        self.mHp = dc['mHp']
        self.vs = dc['vs']
        self.m12sq = dc['m12sq']
        self.calc_rot_matrix()

    def calc_rot_matrix(self):
        c = np.cos
        s = np.sin
        a1 = self.al1
        a2 = self.al2
        a3 = self.al3
        R = np.zeros(shape=(3, 3))
        R[0, 0] = c(a1) * c(a2)
        R[0, 1] = c(a2) * s(a1)
        R[0, 2] = s(a2)
        R[1, 0] = -(c(a3) * s(a1)) - c(a1) * s(a2) * s(a3)
        R[1, 1] = c(a1) * c(a3) - s(a1) * s(a2) * s(a3)
        R[1, 2] = c(a2) * s(a3)
        R[2, 0] = -(c(a1) * c(a3) * s(a2)) + s(a1) * s(a3)
        R[2, 1] = -(c(a3) * s(a1) * s(a2)) - c(a1) * s(a3)
        R[2, 2] = c(a2) * c(a3)
        self.R = R

    def get_phi_vevs(self):
        beta = np.arctan(self.tb)
        self.cb = np.cos(beta)
        self.sb = np.sin(beta)
        self.v1 = self.v * self.cb
        self.v2 = self.v * self.sb

    def get_lambdas(self):
        mH1 = self.mH1
        mH2 = self.mH2
        mH3 = self.mH3
        m12sq = self.m12sq
        tb = self.tb
        R = self.R
        v = self.v
        mHp = self.mHp
        mA = self.mA
        vs = self.vs
        self.lam1 = (
            (1. + tb**2) * (
                -(m12sq * tb) +
                mH1**2 * R[0, 0]**2 +
                mH2**2 * R[1, 0]**2 +
                mH3**2 * R[2, 0]**2)) / v**2
        self.lam2 = ((1. + tb**2) * (
            -m12sq +
            mH1**2 * tb * R[0, 1]**2 +
            mH2**2 * tb * R[1, 1]**2 +
            mH3**2 * tb * R[2, 1]**2)) / (tb**3 * v**2)
        self.lam3 = (
            2. * mHp**2 * tb -
            m12sq * (1. + tb**2) +
            (1. + tb**2) * (
                mH1**2 * R[0, 0] * R[0, 1] +
                mH2**2 * R[1, 0] * R[1, 1] +
                mH3**2 * R[2, 0] * R[2, 1])) / (tb * v**2)
        self.lam4 = (
            m12sq + mA**2 * tb -
            2. * mHp**2 * tb +
            m12sq * tb**2) / (tb * v**2)
        self.lam5 = (
            m12sq - mA**2 * tb + m12sq * tb**2) / (tb * v**2)
        self.lam6 = (
            mH1**2 * R[0, 2]**2 +
            mH2**2 * R[1, 2]**2 +
            mH3**2 * R[2, 2]**2) / vs**2
        self.lam7 = (np.sqrt(1. + tb**2) * (
            mH1**2 * R[0, 0] * R[0, 2] +
            mH2**2 * R[1, 0] * R[1, 2] +
            mH3**2 * R[2, 0] * R[2, 2])) / (v * vs)
        self.lam8 = (np.sqrt(1. + tb**2) * (
            mH1**2 * R[0, 1] * R[0, 2] +
            mH2**2 * R[1, 1] * R[1, 2] +
            mH3**2 * R[2, 1] * R[2, 2])) / (tb * v * vs)

    def get_bilinears(self):
        L1 = self.lam1
        L2 = self.lam2
        L3 = self.lam3
        L4 = self.lam4
        L5 = self.lam5
        L6 = self.lam6
        L7 = self.lam7
        L8 = self.lam8
        v1 = self.v1
        v2 = self.v2
        vs = self.vs
        m12sq = self.m12sq
        self.m11sq = -(L1 * v1**2) / 2. + (m12sq * v2) / v1 - \
            (L3 * v2**2) / 2. - (L4 * v2**2) / 2. - \
                (L5 * v2**2) / 2. - (L7 * vs**2) / 2.
        self.m22sq = -(L3 * v1**2) / 2. - (L4 * v1**2) / 2. - \
            (L5 * v1**2) / 2. + (m12sq * v1) / v2 - \
                (L2 * v2**2) / 2. - (L8 * vs**2) / 2.
        self.mXsq = self.mXi**2
        self.mSsq = self.mXsq - L7 * v1**2 - L8 * v2**2 - L6 * vs**2

    def check_masses(self):

        L1 = self.lam1
        L2 = self.lam2
        L3 = self.lam3
        L4 = self.lam4
        L5 = self.lam5
        L6 = self.lam6
        L7 = self.lam7
        L8 = self.lam8

        tb = self.tb
        sb = self.sb
        cb = self.cb

        v = self.v
        vs = self.vs
        m12sq = self.m12sq

        R = self.R

        # CP evens
        M = np.zeros(shape=(3, 3))
        M[0, 0] = L1 * v**2 * cb**2 + m12sq * tb
        M[0, 1] = -m12sq + L3 * v**2 * cb * sb + \
            L4 * v**2 * cb * sb + L5 * v**2 * cb * sb
        M[1, 0] = M[0, 1]
        M[0, 2] = L7 * v * vs * cb
        M[2, 0] = M[0, 2]
        M[1, 1] = m12sq / tb + L2 * v**2 * sb**2
        M[1, 2] = L8 * v * vs * sb
        M[2, 1] = M[1, 2]
        M[2, 2] = L6 * vs**2
        diag = np.dot(np.dot(R, M), R.T)
        M1 = np.sqrt(diag[0, 0])
        M2 = np.sqrt(diag[1, 1])
        M3 = np.sqrt(diag[2, 2])
        if not np.allclose(
            np.array([M1, M2, M3]),
            np.array([self.mH1, self.mH2, self.mH3])):
            raise RuntimeError(
                'Problem with input: Scalar masses do not fit.')

        # CP odds
        M = np.zeros(shape=(2, 2))
        M[0, 0] = -L5 * v**2 * sb**2 + m12sq * tb
        M[0, 1] = -m12sq + L5 * v**2 * cb * sb
        M[1, 0] = M[0, 1]
        M[1, 1] = -L5 * v**2 * cb**2 + m12sq / tb
        Rp = np.array([[cb, sb], [-sb, cb]])
        diag = np.dot(np.dot(Rp, M), Rp.T)
        M1 = np.sqrt(abs(diag[0, 0]))
        M2 = np.sqrt(diag[1, 1])
        if not np.allclose(
            np.array([M1, M2]),
            np.array([0.0, self.mA]),
            atol=1e-3): # Use atol here because comparing to zero
            raise RuntimeError(
                'Problem with input: Pseudoscalar masses do not fit.')

        # Chargeds
        M = np.zeros(shape=(2, 2))
        M[0, 0] = -L4 * v**2 * sb**2 / 2 - \
            L5 * v**2 * sb**2 / 2 + m12sq * tb
        M[0, 1] = -m12sq + L4 * v**2 * sb * cb / 2 + \
            L5 * v**2 * sb * cb / 2
        M[1, 0] = M[0, 1]
        M[1, 1] = -L4 * v**2 * cb**2 / 2 - \
            L5 * v**2 * cb**2 / 2 + m12sq / tb
        Rp = np.array([[cb, sb], [-sb, cb]])
        diag = np.dot(np.dot(Rp, M), Rp.T)
        M1 = np.sqrt(abs(diag[0, 0]))
        M2 = np.sqrt(diag[1, 1])
        if not np.allclose(
            np.array([M1, M2]),
            np.array([0.0, self.mHp]),
            atol=1e-3): # Use atol here because comparing to zero
            raise RuntimeError(
                'Problem with input: Charged scalar masses do not fit.')
