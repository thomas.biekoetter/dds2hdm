# dds2hdm

A python package for the calculation
of one-loop contributions to the
scattering cross section of dark matter
on nuclei in
the singlet-extended 2 Higgs doublet
model (S2HDM).

The documentation of `dds2hdm` does not
exist yet.

## Authors

`dds2hdm` is developed by [Thomas Biekötter].

## Citation guide

If you use this code for a scientific
publication, please cite the following
paper:

* [arXiv:2207.04973]: Thomas Biekötter, María
    Olalla Olea, Pedro Gabriel and Rui Santos,
    Direct detection of pseudo-Nambu-Goldstone dark matter
    in a two Higgs doublet plus singlet extension of the SM

## Installation

The package and all its dependencies
are installed by doing:

```
make all
```




<!-- Links -->
[Thomas Biekötter]: mailto:thomas.biekoetter@desy.de
[arXiv:2207.04973]: https://arxiv.org/abs/2207.04973
